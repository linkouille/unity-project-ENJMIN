using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boid : MonoBehaviour
{
    [SerializeField] private float maxSpeed = 5;
    [SerializeField] private float maxForce = 0.3f;
    [SerializeField] private Vector4 wallLimit;

    private Rigidbody rb;

    private Vector3 acc;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private Vector3 Seek(Vector3 target)
    {
        Vector3 desired = (target - transform.position).normalized * maxSpeed;
        Vector3 steer = Vector3.ClampMagnitude(desired - rb.velocity, maxForce);
        return steer;
    }

    private bool avoidWalls()
    {

        if (transform.position.x > wallLimit.x)
        {
            Vector3 desired = new Vector3(-maxSpeed, rb.velocity.z);
            Vector3 steer = Vector3.ClampMagnitude(desired - rb.velocity, maxForce);
            ApplyForce(steer);
            return false;
        }
        if (transform.position.x < wallLimit.y)
        {
            Vector3 desired = new Vector3(maxSpeed,rb.velocity.z);
            Vector3 steer = Vector3.ClampMagnitude(desired - rb.velocity, maxForce);
            ApplyForce(steer);
            return false;
        }
        if (transform.position.x < wallLimit.z)
        {
            Vector3 desired = new Vector3(rb.velocity.x, maxSpeed);
            Vector3 steer = Vector3.ClampMagnitude(desired - rb.velocity, maxForce);
            ApplyForce(steer);
            return false;
        }
        if (transform.position.x > wallLimit.w)
        {
            Vector3 desired = new Vector3(rb.velocity.x, -maxSpeed);
            Vector3 steer = Vector3.ClampMagnitude(desired - rb.velocity, maxForce);
            ApplyForce(steer);
            return false;
        }

        return true;
    }

    private void ApplyForce(Vector3 force)
    {
        acc += force;
    }
    private void Update()
    {

    }

    private void Flock(Boids boids)
    {

    }
}
