using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public struct Agent
{
    public Vector3 pos;
    public Vector3 vel;
    public Vector3 acc;
    public Vector4 color;
};

public class Boids : MonoBehaviour
{
    [SerializeField] private Transform camera;

    [SerializeField] private int n;
    [SerializeField] private Transform agent;

    [SerializeField] private List<Transform> agents;

    [SerializeField] private float[] wallLimitX;
    [SerializeField] private float[] wallLimitY;
    [SerializeField] private float[] wallLimitZ;

    [SerializeField] private float maxSpeed = 5;
    [SerializeField] private float maxForce = 0.3f;
    [SerializeField] private float distanceMin;
    [SerializeField] private float distanceVoisin;
    [SerializeField] private float sepMult;
    [SerializeField] private float coheMult;
    [SerializeField] private float alignMult;

    private Agent[] data;

    [SerializeField] private ComputeShader computeAgent;

    private void Start()
    {

        for (int i = 0; i < n; i++)
        {
            agents.Add( GameObject.Instantiate(agent,transform));
        }

        data = new Agent[agents.Count];
        for (int i = 0; i < n; i++)
        {
            data[i].pos = new Vector3(Random.Range(wallLimitX[0], wallLimitX[1]), Random.Range(wallLimitY[0], wallLimitY[1]), Random.Range(wallLimitZ[0], wallLimitZ[1]));
            data[i].vel = new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), Random.Range(-1, 1));
            data[i].vel.Normalize();
            data[i].acc = Vector3.zero;
        }
    }




    private void Update()
    {
        ComputeBuffer agentBuffer = new ComputeBuffer(data.Length, sizeof(float) * 13);

        agentBuffer.SetData(data);

        computeAgent.SetBuffer(0, "agents", agentBuffer);

        computeAgent.SetInt("n", n);

        computeAgent.SetFloat("maxSpeed", maxSpeed);
        computeAgent.SetFloat("maxForce", maxForce);
        computeAgent.SetFloat("distanceMin", distanceMin);
        computeAgent.SetFloat("distanceVoisin", distanceVoisin);

        computeAgent.SetFloat("sepMult", sepMult);
        computeAgent.SetFloat("coheMult", coheMult);
        computeAgent.SetFloat("alignMult", alignMult);

        computeAgent.SetFloats("wallDistX", wallLimitX);
        computeAgent.SetFloats("wallDistY", wallLimitY);
        computeAgent.SetFloats("wallDistZ", wallLimitZ);

        computeAgent.Dispatch(0, data.Length / 10, 1, 1);

        agentBuffer.GetData(data);

        Vector3 pos = Vector3.zero;
        for (int i = 0; i < n; i++)
        {
            Agent a = data[i];
            agents[i].position = a.pos;
            pos += a.pos;
            if(a.vel != Vector3.zero)
                agents[i].rotation = Quaternion.LookRotation(a.vel.normalized);

            TrailRenderer tR = agents[i].GetComponentInChildren<TrailRenderer>();
            tR.startColor = a.color;

            Debug.DrawRay(agents[i].position, a.vel, Color.red);
        }

        if(camera != null)
        {
            Vector3 dir = -(camera.position - pos / n).normalized;
            camera.rotation = Quaternion.LookRotation(dir, Vector3.up);
        }
        
    }
}
