using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetPlacer : MonoBehaviour
{

    [SerializeField] private Transform targetL;
    [SerializeField] private Transform targetR;
    [Range(0,1f)]
    [SerializeField] private float espacement;
    [Range(0, 10f)]
    [SerializeField] private float height;
    [Range(0, 1f)]
    [SerializeField] private float stepRange;
    [Range(0, 1f)]
    [SerializeField] private float stepEspacement;


    [SerializeField] private LayerMask groundMask;

    private RaycastHit[] hitL;

    private RaycastHit[] hitR;

    private bool moveL;

    private bool moveR;

    private float timeL;
    private float timeR;

    private void Start()
    {
        hitR = Physics.RaycastAll(new Ray(transform.position + transform.right * espacement, -transform.up), 999, groundMask);
        hitL = Physics.RaycastAll(new Ray(transform.position - transform.right * espacement, -transform.up), 999, groundMask);

        targetR.position = hitR[0].point + transform.forward * stepEspacement/2;
        targetL.position = hitL[0].point - transform.forward * stepEspacement/2;

    }

    private void FixedUpdate()
    {
        ComputeRayCast(height);

        if(hitR.Length != 0 && Vector3.Distance(targetR.position, hitR[0].point) > stepRange && !moveL)
        {
            targetR.position = hitR[0].point + transform.forward * stepEspacement;
        }

        
        if (hitL.Length != 0 && Vector3.Distance(targetL.position, hitL[0].point) > stepRange && !moveR)
        {
            targetL.position = hitL[0].point + transform.forward * stepEspacement;
        }

    }

    private void ComputeRayCast(float h)
    {
        hitR = Physics.RaycastAll(new Ray(transform.position + transform.right * espacement, -transform.up), h, groundMask);
        hitL = Physics.RaycastAll(new Ray(transform.position - transform.right * espacement, -transform.up), h, groundMask);
    }
    /*
    private void OnDrawGizmos()
    {
        Debug.DrawRay(transform.position + transform.right * espacement, -transform.up);
        Debug.DrawRay(transform.position - transform.right * espacement, -transform.up);

        if(hitL.Length > 0)
            Gizmos.DrawWireSphere(hitL[0].point,0.1f);
    }*/

}
