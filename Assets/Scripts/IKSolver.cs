using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKSolver : MonoBehaviour
{

    [SerializeField] private Transform root;

    [SerializeField] private Transform tip;

    [SerializeField] private Transform target;

    [SerializeField] private Transform pole;

    [SerializeField] private int iterations = 10;

    private int chainLenght;

    private Transform[] bones;
    private float[] bonesLength;
    private Vector3[] bonesPosition;

    private float totalLenght = 0;

    private void Start()
    {
        Init();
    }

    private void Init()
    {
        // Find root from tips parent
        Transform parent = tip;
        chainLenght = 0;
        while(parent != null && parent != root)
        {
            chainLenght++;
            parent = parent.parent;
        }

        if(parent == null)
        {
            throw new UnityException("Root not found in tip parenting");
        }


        bones = new Transform[chainLenght + 1];
        bonesLength = new float[chainLenght];
        bonesPosition = new Vector3[chainLenght + 1];

        InitArrays();
    }

    private void InitArrays()
    {
        Transform current = tip;
        for (int i = chainLenght; i >= 0; i--)
        {
            bones[i] = current;
            bonesPosition[i] = current.position;
            if (i < chainLenght)
            {
                bonesLength[i] = Vector3.Distance(current.position, bones[i + 1].position);
                totalLenght += bonesLength[i];
            }
            current = current.parent;
        }
    }

    private void LateUpdate()
    {
        InitArrays();
        SolveIK();
        ApplyTransform();
    }

    private void SolveIK()
    {
        if (target == null)
            return;

        if (Vector3.Distance(target.position, root.position) > totalLenght)
        {
            Vector3 dirtoTarget = (target.position - root.position).normalized;

            for (int i = 1; i < chainLenght + 1; i++)
            {
                bonesPosition[i] = bonesPosition[i - 1] + dirtoTarget * bonesLength[i - 1];
            }

        }else
        {
            for (int it = 0; it < iterations; it++)
            {

                Vector3 midPoint;
                Vector3 rootPos = bonesPosition[0];
                // Backward
                bonesPosition[chainLenght] = target.position;
                

                for (int i = chainLenght - 1;  i >= 0; i--)
                {
                    Vector3 dir = (bonesPosition[i] - bonesPosition[i + 1]).normalized;
                    Vector3 dirToPole = -(bonesPosition[i] - pole.position).normalized;
                    dir += dirToPole * 0.07f;
                    dir.Normalize();
                    bonesPosition[i] = bonesPosition[i + 1]  + dir * bonesLength[i];
                }

                // Forward
                bonesPosition[0] = rootPos;
                for (int i = 1; i < chainLenght+1; i++)
                {

                    Vector3 dir = (bonesPosition[i] - bonesPosition[i - 1]).normalized;
                    Vector3 dirToPole = -(bonesPosition[i] - pole.position).normalized;
                    dir += dirToPole * 0.07f;
                    dir.Normalize();
                    bonesPosition[i] = bonesPosition[i - 1] + dir * bonesLength[i - 1];
                }
            }
        }
    }

    private void ApplyTransform()
    {
        /*for (int i = 0; i < chainLenght-1; i++)
        {
            Vector3 dirToNext = (bonesPosition[i] - bonesPosition[i + 1]).normalized;
            bones[i].rotation = Quaternion.LookRotation(dirToNext);
        }*/

        for (int i = 1; i < chainLenght + 1; i++)
        {
            bones[i].position = bonesPosition[i];
        }
    }
}
