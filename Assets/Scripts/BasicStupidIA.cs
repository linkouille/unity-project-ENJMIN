using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicStupidIA : MonoBehaviour
{

    [SerializeField] private float speed;
    [SerializeField] private float rotaSpeed;
    [SerializeField] private LayerMask groundLayer;

    private Rigidbody rb;
    private Collider col;

    private Vector3 dir;

    private bool grounded;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();
    }

    private void Update()
    {
        grounded = IsGrounded();
    }

    private void FixedUpdate()
    {
        dir = transform.TransformDirection(Vector3.forward);

        if (grounded)
            rb.velocity = dir * speed;

        RaycastHit h;
        if (Physics.Raycast(transform.position, transform.forward, out h, 1))
        {
            float angleV = Vector3.Angle(h.normal, -dir);
            Vector3 newDir;
            if (h.normal == -dir)
            {
                newDir = -dir;
            }
            else
            {
                newDir = new Vector3(dir.x * Mathf.Cos(-1 * angleV * Mathf.Deg2Rad) - dir.z * Mathf.Sin(-1 * angleV * Mathf.Deg2Rad), 
                    -dir.y, dir.x * Mathf.Sin(-1 * angleV * Mathf.Deg2Rad) + dir.z * Mathf.Cos(-1 * angleV * Mathf.Deg2Rad));
            }

            transform.rotation = Quaternion.LookRotation(newDir);
        }

    }

    public bool IsGrounded()
    {
        RaycastHit[] hits = Physics.RaycastAll(transform.position + (col.bounds.extents.y) * Vector3.down, Vector3.down, 1, groundLayer);
        return hits.Length > 0;
    }

}
